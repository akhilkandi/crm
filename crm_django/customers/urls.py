from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from .views import customers_list, customers_detail
from .models import Customer

urlpatterns=[
url(r'^api/customers/$', customers_list),
url(r'^api/customers/(?P<pk>[0-9]+)$', customers_detail),

]
